package me.gof.pattern.proxy.example;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class Client {
    public static void main(String[] args) {
        Tank t = new Tank();
        Moveable moveable = new TanktimeProxy(t);
        Moveable moveableLog = new TanklogProxy(moveable);

        moveableLog.move();

    }
}
