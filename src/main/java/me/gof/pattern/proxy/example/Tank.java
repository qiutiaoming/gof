package me.gof.pattern.proxy.example;

import java.util.Random;

/**
 * @author qiutiaoming on 14-12-2.
 * @email qtm315@qq.com
 */
public class Tank implements Moveable{

    @Override
    public void move() {
        System.out.println("Tank moving ...");
        try {
            Thread.sleep(new Random().nextInt(5));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
