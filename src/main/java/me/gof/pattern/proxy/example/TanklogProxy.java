package me.gof.pattern.proxy.example;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class TanklogProxy implements Moveable {
    private Moveable m;

    public TanklogProxy(Moveable m) {
        super();
        this.m = m;
    }

    @Override
    public void move() {
        System.out.println("start move....");
        m.move();
        System.out.println("end move....");

    }
}
