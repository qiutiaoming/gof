package me.gof.pattern.proxy.example;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class TanktimeProxy implements Moveable {
    private Moveable t;

    public TanktimeProxy(Moveable t) {
        super();
        this.t = t;
    }

    @Override
    public void move() {
        long start = System.currentTimeMillis();
        System.out.println("start at: " + start);

        t.move();

        long end = System.currentTimeMillis();
        System.out.println("end at: " + end);

        System.out.println("operation time:" + (end - start));

    }
}
