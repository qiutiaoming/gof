package me.gof.pattern.proxy.example1;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public interface BusinessBar {
    String bar(String message);
}
