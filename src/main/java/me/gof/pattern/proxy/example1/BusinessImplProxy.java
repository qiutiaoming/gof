package me.gof.pattern.proxy.example1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class BusinessImplProxy implements InvocationHandler {
    private Object obj;

    public BusinessImplProxy() {
    }

    public BusinessImplProxy(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        doBefore();
        result = method.invoke(obj, args);
        doAfter();
        return result;
    }

    private void doAfter() {
        System.out.println("do something after business logical.");
    }

    private void doBefore() {
        System.out.println("do something before business logical.");
    }

    public static Object factory(Object obj) {
        Class clazz = obj.getClass();
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), new BusinessImplProxy(obj));
    }
}
