package me.gof.pattern.proxy.example1;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class BusinessFooImpl implements BusinessFoo {
    @Override
    public void foo() {
        System.out.println("BusinessFooImpl.foo() invoked");
    }
}
