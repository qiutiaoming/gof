package me.gof.pattern.proxy.example1;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class Client {
    public static void main(String[] args) {
        BusinessFooImpl businessFooImpl = new BusinessFooImpl();
        BusinessFoo businessFoo = (BusinessFoo) BusinessImplProxy.factory(businessFooImpl);

        businessFoo.foo();
        System.out.println();

        BusinessBarImpl businessBarImpl = new BusinessBarImpl();
        BusinessBar businessBar = (BusinessBar) BusinessImplProxy.factory(businessBarImpl);

        String hello = businessBar.bar("hello,world");

        System.out.println(hello);

    }
}
