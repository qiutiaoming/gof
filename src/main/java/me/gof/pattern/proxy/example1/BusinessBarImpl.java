package me.gof.pattern.proxy.example1;

/**
 * @Author: qiutiaoming on 14-12-2.
 * @E-mail: qtm315@qq.com
 */
public class BusinessBarImpl implements BusinessBar {
    public String bar(String message) {
        System.out.println("BusinessBarImpl.bar() invoked.");
        return message;
    }
}
