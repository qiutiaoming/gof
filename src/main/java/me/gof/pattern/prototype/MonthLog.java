package me.gof.pattern.prototype;

/**
 * @Author: qiutiaoming on 14-11-28.
 * @E-mail: qtm315@qq.com
 */
public class MonthLog implements Cloneable {

    private String name;
    private String date;
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MonthLog() {
        System.out.println(this.getClass().getName() +  " inited");
    }

    protected MonthLog clone() {
        try {
            return (MonthLog) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        return "MonthLog{" +
            "name='" + name + '\'' +
            ", date='" + date + '\'' +
            ", content='" + content + '\'' +
            '}';
    }
}
