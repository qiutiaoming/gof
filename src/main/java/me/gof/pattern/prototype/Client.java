package me.gof.pattern.prototype;

/**
 * @Author: qiutiaoming on 14-11-28.
 * @E-mail: qtm315@qq.com
 */
public class Client {
    public static void main(String[] args) {
        MonthLog previous = new MonthLog();
        previous.setName("zhangsan");
        previous.setDate("2014-10-11");
        previous.setContent("压力山大");

        // 直接复制上一个对象然后修改
        MonthLog newLog = previous.clone();
        newLog.setName("lisi");

        System.out.println(previous);
        System.out.println(newLog);

    }
}
