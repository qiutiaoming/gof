package me.gof.pattern.composite.example1;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Folder implements Root {
    String name;
    List<Root> folders;

    public Folder(String name) {
        this.name = name;
        this.folders = new ArrayList<Root>();
    }

    @Override
    public boolean addFile(Root file) {

        return folders.add(file);
    }

    @Override
    public boolean removeFile(Root file) {

        return folders.remove(file);
    }

    @Override
    public List<Root> getFiles() {
        return folders;
    }

    @Override
    public void display() {
        System.out.println(name);

        for (Root file : folders) {
            if (file instanceof Folder) {
                System.out.print("    ");
            }
            file.display();
        }


    }
}
