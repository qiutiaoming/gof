package me.gof.pattern.composite.example1;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Client {
    public static void main(String[] args) {
        Root root1 = new Folder("/user");
        Root document = new Folder("/document");
        Root download = new Folder("/download");
        Root somethings = new Folder("/somethings");
        Root hello = new File("Helloworld.java");

        somethings.addFile(hello);
        download.addFile(somethings);

        root1.addFile(document);
        root1.addFile(download);

        root1.display();
    }
}
