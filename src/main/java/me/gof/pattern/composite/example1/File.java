package me.gof.pattern.composite.example1;

import java.util.List;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class File implements Root{
    String name;

    public File(String name) {
        this.name = name;
    }

    @Override
    public boolean addFile(Root file) {
        return false;
    }

    @Override
    public boolean removeFile(Root file) {
        return false;
    }

    @Override
    public List<Root> getFiles() {
        return null;
    }

    @Override
    public void display() {
        System.out.println("       " + name);

    }
}
