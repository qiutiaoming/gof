package me.gof.pattern.composite.example1;

import java.util.List;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 * Root: produce File and Folder,as Component
 * Folder: as Composite
 * File : as Leaf
 * Client : as client
 *
 */
public interface Root {
    boolean addFile(Root file);
    boolean removeFile(Root file);
    List<Root> getFiles();
    void display();
}
