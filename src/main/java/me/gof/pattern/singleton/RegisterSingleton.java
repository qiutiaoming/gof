package me.gof.pattern.singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: qiutiaoming on 14-11-25.
 * @E-mail: qtm315@qq.com
 */
public class RegisterSingleton {
    private static Map<String, RegisterSingleton> registerSingletonMap = new HashMap();

    static {
        RegisterSingleton registerSingleton = new RegisterSingleton();
        registerSingletonMap.put(registerSingleton.getClass().getName(), registerSingleton);
    }

    private RegisterSingleton() {
    }

    public static RegisterSingleton getInstance(String name) {
        if (name == null) {
            name = RegisterSingleton.class.getName();
        }
        if (registerSingletonMap.get(name) == null) {
            try {
                registerSingletonMap.put(name, (RegisterSingleton) Class.forName(name).newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return registerSingletonMap.get(name);
    }


}
