package me.gof.pattern.singleton;

/**
 * @Author: qiutiaoming on 14-11-26.
 * @E-mail: qtm315@qq.com
 */
public class MixSingleton {

    private MixSingleton() {
        System.out.println("create");
    }

    private static class SingletonHolder {
        private static MixSingleton instance = new MixSingleton();
    }

    public static MixSingleton getInstance() throws InterruptedException {
        Thread.sleep(100);
        return SingletonHolder.instance;
    }

    public static void main(String[] args) throws Exception{

        //System.out.println(MixSingleton.getInstance());

        System.out.println("hello,world");

        Thread t1 = new Thread(){
            public void run() {
                try {
                    System.out.println(MixSingleton.getInstance());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

       Thread t2 =  new Thread(){
            public void run() {
                try {
                    System.out.println(MixSingleton.getInstance());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        t1.start();
        t2.start();



    }
}
