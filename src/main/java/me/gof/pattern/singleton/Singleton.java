package me.gof.pattern.singleton;

/**
 * @Author: qiutiaoming on 14-11-25.
 * @E-mail: qtm315@qq.com
 */
public class Singleton {
    // init after the object is created
    private static Singleton instance = new Singleton();
    //override default constructor
    private Singleton(){}

    public static Singleton getInstance() {
        return instance;
    }

}
