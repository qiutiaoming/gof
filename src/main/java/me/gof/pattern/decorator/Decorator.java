package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public abstract class Decorator implements  Component{

    private Component component;

    public void setComponent(Component component) {
        this.component = component;
    }

    @Override
    public void showCoffee() {
        component.showCoffee();
    }

    @Override
    public double showPrice() {
        return  component.showPrice();
    }
}
