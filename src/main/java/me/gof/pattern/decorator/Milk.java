package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Milk extends Decorator {
    @Override
    public void showCoffee() {
        System.out.print("加奶");
        super.showCoffee();
    }

    @Override
    public double showPrice() {
        return super.showPrice() + 10.00;
    }
}
