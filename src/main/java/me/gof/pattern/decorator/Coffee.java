package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Coffee implements Component {

    private String name;
    private double price;

    public Coffee(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void showCoffee() {
        System.out.println("的" + this.getName() + "咖啡");

    }

    @Override
    public double showPrice() {
        return this.getPrice();
    }
}
