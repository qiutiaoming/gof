package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Client {

    public static void main(String[] args) {
        Coffee coffee = new Coffee("南山", 55.00);
        Decorator sugar = new Sugar();
        Decorator milk = new Milk();

        //指代不清晰
        // TODO: why not?
        // coffee.setComponent(sugar).setComponent(milk);
        // System.out.println(coffee.showPrice());

        sugar.setComponent(coffee);
        //milk.setComponent(sugar);

        sugar.showCoffee();

        System.out.println(sugar.showPrice());
    }
}
