package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public class Sugar extends Decorator {

    @Override
    public void showCoffee() {
        System.out.print("加糖");
        super.showCoffee();
    }

    @Override
    public double showPrice() {
        return super.showPrice() + 15.00;
    }
}
