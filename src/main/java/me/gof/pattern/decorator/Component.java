package me.gof.pattern.decorator;

/**
 * @Author: qiutiaoming on 14-12-1.
 * @E-mail: qtm315@qq.com
 */
public interface Component {
    void showCoffee();
    double showPrice();
}
