package me.gof.pattern.singleton;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @Author: qiutiaoming on 14-11-25.
 * @E-mail: qtm315@qq.com
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({LazySingletonTest.class, SingletonTest.class, RegisterSingletonTest.class})

public class SingletonTestSuite {
}
