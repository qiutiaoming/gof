package me.gof.pattern.singleton;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegisterSingletonTest {

    @Test
    public void testGetInstance() throws Exception {
        RegisterSingleton obj1 = RegisterSingleton.getInstance(RegisterSingleton.class.getName());
        RegisterSingleton obj2 = RegisterSingleton.getInstance(RegisterSingleton.class.getName());


        assertEquals("RegisterSingleton object is not equal",obj1,obj2);


    }
}
