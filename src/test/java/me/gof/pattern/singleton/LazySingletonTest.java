package me.gof.pattern.singleton;

import org.junit.Test;

import static org.junit.Assert.*;

public class LazySingletonTest {

    @Test
    public void testGetInstance() throws Exception {

        LazySingleton obj1 = LazySingleton.getInstance();
        LazySingleton obj2 = LazySingleton.getInstance();

        assertEquals("failed object are not equal",obj1,obj2);

    }
}
