package me.gof.pattern.singleton;

import org.junit.Test;

import static org.junit.Assert.*;

public class SingletonTest {

    @Test
    public void testGetInstance() throws Exception {
        Singleton obj1 = Singleton.getInstance();
        Singleton obj2 = Singleton.getInstance();

        assertEquals("failure - object are not equal", obj1, obj2);
    }
}
